# Hello World

Simple "Hello World" application for testing purposes.

## Usage

### Build a Docker image

```shell
$ docker build -t hello-world .
```

### Run it in Docker

```shell
$ docker run --rm -it -p 8000:8000 hello-world
```
